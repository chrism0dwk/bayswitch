# Bayswitch antenna farm management system

Bayswitch is a [CANbus](https://en.wikipedia.org/wiki/CAN_bus)-based system 
for controlling remote antenna switches from the comfort of a radio operating desk.
It is designed to be web-configured and operated, with a physical control unit offering 
optional manual controls.

## Features

* Main control unit with LCD display, band data input, ethernet and wifi connectivity.
* Remote switching units operated over a CAN bus, allowing daisy chaining of switches,
rotators, azimuth/elevation, weather sensors, and much more.
* Web control via a web interface.
* Physical control via 8 illuminated front panel switches on the main control unit.
* Free open-source designs, enabling others to contribute extensions.

## Development stage

Bayswitch is currently in a _very_ early stage of development.  The repository owner
is currently playing with homebrew PCB and surface mount technology.  He's mainly a 
statistician and software developer, so hardware production is something (reasonably) new!

