CANtenna web-based control interface
===============================================

Developer installation
--------------------------

Build the Python virtual environment using Poetry
```bash
$ poetry install
```

Running the development server
------------------------------------

```bash
$ poetry run flask --app cantenna_web.main --debug run
```
