from flask import Flask
from flask import Response
from flask import render_template
from flask import request
from flask_sock import Sock
from flask_sock import ConnectionClosed

import json

from cantenna_web.spmt_switch import SP8T_switch, InvalidThrowError

app = Flask(__name__)
app.logger.setLevel(10)
app.config["SOCK_SERVER_OPTIONS"] = {"ping_interval": 25}
sock = Sock(app)

websocket_registry = []


spmt_switch = SP8T_switch("switch0")


@app.route("/")
def index():
    return render_template("switch.html")


def set_switch_and_notify(pole):

    spmt_switch.set_active_throw(pole)

    html = render_switch_panel()
    for ws in websocket_registry:
        ws.send(html)


def render_switch_panel():
    return render_template("switch-array.html", switch=spmt_switch)


@app.route("/api/v1/set-switch", methods=["POST"])
def api_set_switch():

    try:
        set_switch_and_notify(request.form["throw"])
    except InvalidThrowError:
        return (
            render_template(
                "switch_bad_throw.html",
                requested_throw=request.form["throw"],
                switch=spmt_switch,
            ),
            418,
        )
    except KeyError:
        return (
            render_template(
                "bad_request.html", message="Form data 'throw' not received"
            ),
            400,
        )

    return "OK", 200


@sock.route("/ws")
def sock_set_switch(sock):

    app.logger.info("Opening socket %s", sock)

    # Create socket and send relevant data to the
    # new listener.
    websocket_registry.append(sock)
    sock.send(render_switch_panel())

    try:
        while True:
            msg = sock.receive()
            data = json.loads(msg)
            app.logger.info(
                "Manual switch from %s to pole %s", sock, data["pole"]
            )
            set_switch_and_notify(data["pole"])

    except ConnectionClosed:
        app.logger.info("Closing socket %s", sock)
        websocket_registry.remove(sock)
