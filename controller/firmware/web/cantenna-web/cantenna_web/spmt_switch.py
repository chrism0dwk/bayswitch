"""Provides a driver for a CAN-based SPMT switch"""


class InvalidThrowError(Exception):
    """An error selecting particular switch throw"""

    pass


class SP8T_switch:
    def __init__(self, name: str, initial_throw: str = "0"):
        """Models a Single Pole 8 Throw switch

        :param name: The name of the switch
        :param initial_pole: The initial active pole
        """
        self.__name = name
        self.__available_throws = [str(x) for x in range(8)]
        self.set_active_throw(initial_throw)

    @property
    def name(self):
        """:returns: the name of the switch"""
        return self.__name

    @property
    def throws(self):
        """:returns: the available switch positions (throws)"""
        return self.__available_throws

    @property
    def active_throw(self):
        """:returns: the active switch position"""
        return self.__active_throw

    def set_active_throw(self, throw):
        """Sets the active throw

        :param throw: the throw in `throws` to select
        """
        if throw not in self.throws:
            raise InvalidThrowError(f"{throw} not available")

        self.__active_throw = throw

    def is_active(self, throw):
        return throw == self.__active_throw
